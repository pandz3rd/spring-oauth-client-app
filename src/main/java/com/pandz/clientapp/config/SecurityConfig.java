package com.pandz.clientapp.config;

import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
public class SecurityConfig {

  SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.authorizeRequests(auth ->
        auth.anyRequest().authenticated()
    )
    .oauth2Login(oauthLogin ->
        oauthLogin.loginPage("/oauth2/authorization/articles-client-oidc"))
    .oauth2Client(Customizer.withDefaults());

    return httpSecurity.build();
  }
}
